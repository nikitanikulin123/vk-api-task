<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
	protected $fillable = [
		'user_id', 'item_id', 'parent_user_id', 'type', 'link'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
