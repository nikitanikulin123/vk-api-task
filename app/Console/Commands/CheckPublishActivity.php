<?php

namespace App\Console\Commands;

use App\Models\Activity;
use App\User;
use ATehnix\VkClient\Client;
use Illuminate\Console\Command;
use App;

class CheckPublishActivity extends Command
{
    protected $signature = 'publishingActivity:check';
    protected $description = 'Command description';
    protected $counter = 0;

    public function __construct()
    {
        parent::__construct();
    }

	public function handle()
	{
		\DB::connection()->disableQueryLog();
		$api = new Client;
		$api->setDefaultToken(env('VKONTAKTE_SERVICE_KEY', "ce001d63ce001d63ce001d6313ce5fa8cfcce00ce001d63941867c33859502a5f1cfa32"));

		foreach (User::all() as $user) {
			$response = $api->request('wall.search', ['owner_id' => $user->owner_id, 'query' => $user->hash]);
			$dataItems = $response['response']['items'];
			$itemsIds = array_pluck($dataItems, 'id');
			$existedIdsForUser = Activity::whereUserId($user->id)->whereType('publish')->pluck('item_id', 'id')->toArray();
			$newItemsIds = array_diff($itemsIds, $existedIdsForUser);
			foreach ($newItemsIds as $newItemId) {
				$newItem = Activity::create(['user_id' => $user->id, 'item_id' => $newItemId, 'type' => 'publish', 'parent_user_id' => 0,
				                  'link' => "wall{$user->owner_id}_{$newItemId}"]);
				$this->counter++;
				$this->info("{$this->counter}. Created PublishActivity with id: {$newItem->id}");
			}
		}
	}
}
