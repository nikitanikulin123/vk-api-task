<?php

namespace App\Console\Commands;

use App\Models\Activity;
use App\User;
use ATehnix\VkClient\Client;
use Illuminate\Console\Command;
use App;

class CheckRepostCommentLikeActivity extends Command
{
    protected $signature = 'repostCommentLikeActivity:check';
    protected $description = 'Command description';
    protected $counter = 0;

    public function __construct()
    {
        parent::__construct();
    }

	public function handle()
	{
		\DB::connection()->disableQueryLog();
		$api = new Client;
		$api->setDefaultToken(env('VKONTAKTE_SERVICE_KEY', "ce001d63ce001d63ce001d6313ce5fa8cfcce00ce001d63941867c33859502a5f1cfa32"));

		foreach (User::all() as $user) {
			$response = $api->request('wall.get', ['owner_id' => env('VKONTAKTE_GROUP_ID', 81341076), 'count' => 100]);
			$allRoundedItemsCount = ceil($response['response']['count'] / 100);
			$this->storeRepostActivity($api, $user, $response);
			$this->storeCommentActivity($api, $user, $response);
			$this->storeLikeActivity($api, $user, $response);

			if($allRoundedItemsCount > 1) {
				for ($i = 1; $i < $allRoundedItemsCount; $i++) {
					$response = $api->request('wall.get', ['owner_id' => env('VKONTAKTE_GROUP_ID', 81341076), 'count' => 100, 'offset' => 100 * $i]);
					$this->storeRepostActivity($api, $user, $response);
					$this->storeCommentActivity($api, $user, $response);
					$this->storeLikeActivity($api, $user, $response);
				}
			}
		}
	}


	protected function storeLikeActivity($api, $user, $response) {
		$items = $response['response']['items'];
		$itemsWithLikes = array_where($items, function ($value, $key) use ($user) {
			return $value['likes']['count'] !== 0;
		});
		foreach ($itemsWithLikes as $itemWithLikes) {
			$itemId = $itemWithLikes['id'];
			$response = $api->request('likes.getList', ["type" => 'post', "filter" => 'likes', 'owner_id' => env('VKONTAKTE_GROUP_ID', 81341076),
			                                            'count' => 100, 'item_id' => $itemId]);
			$likes = $response['response']['items'];
			$likeOfUser = array_first($likes, function ($value, $key) use ($user) {
				return $value === $user->owner_id;
			});
			if ($likeOfUser) {
				$existedIdsForUser = Activity::whereUserId($user->id)->whereType('like')->whereItemId($itemId)->whereParentUserId(env('VKONTAKTE_GROUP_ID', 81341076))->first();
				if(!$existedIdsForUser) {
					$newItem = Activity::create(['user_id' => $user->id, 'item_id' => $itemId, 'parent_user_id' => env('VKONTAKTE_GROUP_ID', 81341076),
					                             'type' => 'like', 'link' => "wall". env('VKONTAKTE_GROUP_ID', 81341076) . "_{$itemId}"]);
					$this->counter++;
					$this->info("{$this->counter}. Created LikeActivity with id: {$newItem->id}");
				}
			}
		};
	}

	protected function storeCommentActivity($api, $user, $response) {
		$items = $response['response']['items'];
		$itemsWithComments = array_where($items, function ($value, $key) use ($user) {
			return $value['comments']['count'] !== 0;
		});
		foreach ($itemsWithComments as $itemWithComments) {
			$itemId = $itemWithComments['id'];
			$response = $api->request('wall.getComments', ['owner_id' => env('VKONTAKTE_GROUP_ID', 81341076), 'count' => 100, 'post_id' => $itemId]);
			$comments = $response['response']['items'];
			$commentsOfUser = array_where($comments, function ($value, $key) use ($user) {
				return $value['from_id'] == $user->owner_id;
			});
			if (count($commentsOfUser) > 0) {
				$existedIdsForUser = Activity::whereUserId($user->id)->whereType('comment')->whereItemId($itemId)->whereParentUserId(env('VKONTAKTE_GROUP_ID', 81341076))->first();
				if(!$existedIdsForUser) {
					$newItem = Activity::create(['user_id' => $user->id, 'item_id' => $itemId, 'parent_user_id' => env('VKONTAKTE_GROUP_ID', 81341076),
					                             'type' => 'comment', 'link' => "wall". env('VKONTAKTE_GROUP_ID', 81341076) . "_{$itemId}"]);
					$this->counter++;
					$this->info("{$this->counter}. Created CommentActivity with id: {$newItem->id}");
				}

			}
		};
	}

	protected function storeRepostActivity($api, $user, $response) {
		$items = $response['response']['items'];
		$itemsWithReposts = array_where($items, function ($value, $key) use ($user) {
			return $value['reposts']['count'] !== 0;
		});
		foreach ($itemsWithReposts as $itemWithReposts) {
			$response = $api->request('wall.getReposts', ['owner_id' => env('VKONTAKTE_GROUP_ID', 81341076), 'post_id' => $itemWithReposts['id']]);
			$dataProfiles = $response['response']['profiles'];
			$itemId = head($response['response']['items'])['id'];
			$profile = array_where($dataProfiles, function ($value, $key) use ($user) {
				return $value['id'] === $user->owner_id;
			});
			if(count($profile)) {
				$existedIdsForUser = Activity::whereUserId($user->id)->whereType('repost')->whereItemId($itemId)->first();
				if(!$existedIdsForUser) {
					$newItem = Activity::create(['user_id' => $user->id, 'item_id' => $itemId, 'parent_user_id' => 0,
					                             'type' => 'repost', 'link' => "wall{$user->owner_id}_{$itemId}"]);
					$this->counter++;
					$this->info("{$this->counter}. Created RepostActivity with id: {$newItem->id}");
				}

			}
		};
	}
}
