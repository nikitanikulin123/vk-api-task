<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\User;
use ATehnix\VkClient\Client;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function getHome(Request $request)
    {
        $activities = Activity::with('user')->get();
	    return view('home', compact('activities'));
    }
}
