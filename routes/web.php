<?php

Route::get('/', 'PagesController@getHome')->name('home');

Route::get('vkauth', function (\ATehnix\VkClient\Auth $auth) {
	echo "<a href='{$auth->getUrl()}'> Войти через VK.Com </a><hr>";

	if (env('VKONTAKTE_CODE')) {
		echo 'Token: '.$auth->getToken(env('VKONTAKTE_CODE'));
	}
});

