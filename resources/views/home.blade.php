@extends('layouts.app')
@section('content')
	<section class="main">
		<div class="container">
			<h2>Активность пользователей</h2>
			<table class="table">
				<thead>
				<tr>
					<th>#</th>
					<th>Имя</th>
					<th>Тип активности</th>
					<th>Ссылка</th>
					<th>Время активности</th>
				</tr>
				</thead>
				<tbody>
					@foreach($activities as $activity)
						<tr>
							<td>{{ $activity->id }}</td>
							<td>{{ $activity->user->name }}</td>
							<td>{{ $activity->type }}</td>
							<td><a href="https://vk.com/{{ $activity->link }}" target="_blank">https://vk.com/{{ $activity->link }}</a></td>
							<td>{{ $activity->created_at }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</section>
@endsection