<head>
	<link rel="icon" href="/favicon.ico">
	<title>User Activity</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">

    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('css/all.css') }}">

	<!--[if IE 9]>
	<script>
		window.location = "packages/oldbrowser/index.html";
	</script>

	<script src="libs/oldbrowser/html5shiv/es5-shim.min.js"></script>
	<script src="libs/oldbrowser/html5shiv/html5shiv.min.js"></script>
	<script src="libs/oldbrowser/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/oldbrowser/respond/respond.min.js"></script>
	<![endif]-->
</head>