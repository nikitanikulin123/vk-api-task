<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
	    $user = User::create([
            'name' => $faker->name,
            'owner_id' => 377505428,
            'hash' => 'projectx_user_2387348734',
        ]);
    }
}
